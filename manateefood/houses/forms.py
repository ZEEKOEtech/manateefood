from django.forms import ModelForm

from .models import House, Supply


class HouseForm(ModelForm):
    class Meta:
        model = House
        fields = ['name', 'members']

class SupplyForm(ModelForm):
    class Meta:
        model = Supply
        fields = ['amount', 'shopping_date', 'supplier']
