from datetime import date

from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from users.models import ManateeFoodUser

# Create your models here.

class House(models.Model):
    """A top-level entity containing members, supplies, meals and products"""
    name = models.CharField(_("House name"), max_length=30, blank=False)

    members = models.ManyToManyField(ManateeFoodUser, related_name="houses")

    created_at = models.DateTimeField(_("House created at"), auto_now=True)
    updated_at = models.DateTimeField(_("House updated at"), auto_now=True)
    is_active = models.BooleanField(_("House is active"), default=True)


class Supply(models.Model):
    """
    Represents a supply with a shopping date, amount and supplier
    (person that bought the supply). Happens in the context of a House.
    """
    amount = models.IntegerField(_("Supply amount"), help_text=_("Supply amount (CHF cents)"))
    shopping_date = models.DateField(_("Supply shopping date"), default=date.today)

    supplier = models.ForeignKey(ManateeFoodUser, on_delete=models.CASCADE)
    house = models.ForeignKey(House, on_delete=models.CASCADE)

    created_at = models.DateTimeField(_("Supply created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("Supply updated at"), auto_now=True)
    is_active = models.BooleanField(_("Supply is active"), default=True)

class Meal(models.Model):
    """
    Represents a meal in time, with products consumed (MealProductConsumption),
    and users that participate to that meal (MealUserParticipation).
    Happens in the context of a House.
    """
    house = models.ForeignKey(House, on_delete=models.CASCADE)
    datetime = models.DateTimeField(_("Meal approximate date and time"), default=timezone.now)

    created_at = models.DateTimeField(_("Meal created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("Meal updated at"), auto_now=True)
    is_active = models.BooleanField(_("Meal is active"), default=True)

class MealProductConsumption(models.Model):
    """Represents a consumption of a product with an amount. Part of a meal."""
    amount = models.DecimalField(_("Amount of product consumed"), max_digits=6, decimal_places=2, default=0)

    meal = models.ForeignKey(Meal, on_delete=models.CASCADE)
    product = models.ForeignKey("products.Product", on_delete=models.CASCADE)

class MealUserParticipation(models.Model):
    """Represents the participation of a user with a factor. Part of a meal."""
    participation_factor = models.IntegerField(_("Participation factor"), default=0)

    meal = models.ForeignKey(Meal, on_delete=models.CASCADE)
    participant = models.ForeignKey(ManateeFoodUser, on_delete=models.CASCADE)
