from django.conf.urls import url
from . import views

app_name = 'houses'

urlpatterns = [
    url(r'^$', views.all_houses, name="all_houses"),
    url(r'^admin/$', views.admin_show_houses, name="admin_show_houses"),
    url(r'^create/$', views.create_house, name="create_house"),
    url(r'^(?P<pk>[0-9]+)/edit/$', views.edit_house, name="edit_house"),
    url(r'^(?P<pk>[0-9]+)/supplies/$', views.show_supplies, name="show_supplies"),
]
