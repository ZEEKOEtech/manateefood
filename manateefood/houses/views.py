from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required

from app.decorators import superuser_required
from users.models import ManateeFoodUser
from .models import House, Supply
from .forms import HouseForm

# Create your views here.

@login_required
def all_houses(request):
    upk = request.user.pk
    user = ManateeFoodUser.objects.get(pk=upk)
    houses = user.houses.all()

    return render(request, 'houses/all_houses.html', {'houses': houses})

@superuser_required
def admin_show_houses(request):
    houses = House.objects.all()

    return render(request, 'houses/all_houses.html', {'houses': houses})

def create_house(request):
    # After edit
    if request.method == "POST":
        form = HouseForm(request.POST)

        if form.is_valid():
            new_house = form.save()
    # Before edit
    else:
        form = HouseForm()

    #TODO: use a specific create_house template for house creation
    return render(request, 'houses/edit_house.html', {'form':form})


def edit_house(request, pk):
    house = get_object_or_404(House, pk=pk, is_active=True)

    # After edit
    if request.method == "POST":
        form = HouseForm(request.POST, instance=house)

        if form.is_valid():
            new_house = form.save()
    # Before edit
    else:
        form = HouseForm(instance=house)

    return render(request, 'houses/edit_house.html', {'form':form})

def show_supplies(request, pk):
    house = get_object_or_404(House, pk=pk, is_active=True)
    supplies = Supply.objects.filter(pk=pk)

    return render(request, 'houses/show_supplies.html', {'house': house, 'supplies': supplies})
