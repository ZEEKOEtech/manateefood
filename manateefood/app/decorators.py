from django.core.exceptions import PermissionDenied
from django.contrib.auth.decorators import login_required


def superuser_required(function):
    @login_required
    def wrap(request, *args, **kwargs):
        if not request.user.is_superuser:
            raise PermissionDenied

        return function(request, *args, **kwargs)

    return wrap
