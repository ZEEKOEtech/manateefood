from django.contrib.auth.hashers import check_password
from django.shortcuts import get_object_or_404

from .models import ManateeFoodUser

class ManateeFoodAuthBackend(object):
    def authenticate(self, request, email=None, password=None):
        try:
            user = ManateeFoodUser.objects.get(email=email)
            pwd_valid = check_password(password, user.password)
            if pwd_valid:
                return user
        except ManateeFoodUser.DoesNotExist:
            return None

        return None

    def get_user(self, user_id):
        return get_object_or_404(ManateeFoodUser, pk=user_id)
