from django.conf.urls import url
from django.contrib.auth import views as auth_views

from . import views

app_name = 'users'

urlpatterns = [
    url('^login/$', views.login, name="login"),
    url('^register/$', views.register, name="register"),
    url('^logout/$', views.logout, name="logout"),
]