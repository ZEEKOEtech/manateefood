# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-08-30 10:06
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('houses', '0005_auto_20170830_1139'),
        ('users', '0005_remove_manateefooduser_username'),
    ]

    operations = [
        migrations.AddField(
            model_name='manateefooduser',
            name='preferred_house',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='houses.House'),
        ),
    ]
