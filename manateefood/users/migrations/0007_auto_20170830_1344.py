# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-08-30 11:44
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0006_manateefooduser_preferred_house'),
    ]

    operations = [
        migrations.AlterField(
            model_name='manateefooduser',
            name='preferred_house',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='houses.House'),
        ),
    ]
