from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from datetime import datetime

# Create your models here.
class ManateeFoodUserManager(BaseUserManager):
    def _create_user(self, email, password, is_superuser, **extra_fields):
        """Create user with given password"""
        now = timezone.now()
        if not email:
            raise ValueError(_('The email must be set'))

        email = self.normalize_email(email)

        user = self.model(is_active=True, email=email, is_superuser=is_superuser, last_login=now, date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password, is_superuser=False, **extra_fields)

    def create_superuser(self, email, password=None, **extra_fields):
        return self._create_user(email, password, is_superuser=True, **extra_fields)

class ManateeFoodUser(AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(_("First Name"), max_length=50)
    last_name = models.CharField(_("Last Name"), max_length=50)
    email = models.EmailField(_("Email"), max_length=255, blank=False, unique=True)

    preferred_house = models.ForeignKey("houses.House", null=True, blank=True, on_delete=models.SET_NULL)
    date_joined = models.DateTimeField(_("Registration Date"), auto_now_add=True)
    is_active = models.BooleanField(_("Active"), default=True, help_text=_("Defines whether user should be considered active or not. Use this instead of deleting an account."))

    objects = ManateeFoodUserManager()

    USERNAME_FIELD = 'email'
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_full_name(self):
        return "{} {}".format(self.first_name, self.last_name)

    def get_short_name(self):
        return self.email

    def __str__(self):
        return self.email
