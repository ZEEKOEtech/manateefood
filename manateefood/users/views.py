from django.shortcuts import render, redirect
from django.contrib.auth import login as auth_login, logout as auth_logout, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _

from .forms import UserLoginForm, UserCreationForm


# Create your views here.
def login(request):
    if request.POST:
        email = request.POST['email']
        password = request.POST['password']
        user = authenticate(request, email=email, password=password)
        if user is not None:
            messages.success(request, _('You have logged in succesfully.'))
            '''Call django's auth_login (which uses settings.AUTHENTICATION_BACKEND)'''
            info = auth_login(request, user)

            if request.POST['next']:
                return redirect(request.POST['next'])
            else:
                return redirect('main:home')
        else:
            messages.error(request, _('User not found or password did not match'))

    form = UserLoginForm()
    return render(request, 'users/login.html', {'form': form})

def register(request):
    if request.POST:
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            redirect('users:login')
    else:
        form = UserCreationForm()
    return render(request, 'users/register.html', {'form': form})

@login_required
def logout(request):
    auth_logout(request)
    messages.info(request, _('You are now logged out.'))
    return redirect('main:home')
