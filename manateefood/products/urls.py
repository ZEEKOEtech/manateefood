from django.conf.urls import url
from . import views

app_name = 'products'

urlpatterns = [
    url(r'^all/$', views.all_products, name="all_products"),
    url(r'^create/$', views.edit_product, name="create_product"),
    url(r'^edit/(?P<pk>[0-9]+)/$', views.edit_product, name="edit_product"),
]
