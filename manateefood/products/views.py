from django.shortcuts import render, redirect, get_object_or_404

from .models import Product
from .forms import ProductForm

# Create your views here.

def all_products(request):
    products = Product.objects.filter(is_active=True)

    return render(request, 'products/all_products.html', {'products': products})


def edit_product(request, pk=None):
    # New product
    if pk is None:
        product = None
    # Existing product
    else:
        product = get_object_or_404(Product, pk=pk, is_active=True)

    # After edit
    if request.method == "POST":
        form = ProductForm(request.POST, instance=product)

        if form.is_valid():
            new_product = form.save()
            return redirect('products:edit_product', pk=new_product.pk)
        else:
            return render(request, 'products/edit_product.html', {'form':form})

    # Before edit
    else:
        form = ProductForm(instance=product)

    return render(request, 'products/edit_product.html', {'form':form})
