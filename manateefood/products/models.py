from django.db import models
from django.utils.translation import ugettext_lazy as _

from houses.models import House

# Create your models here.

UNIT_CHOICES = (
    ("PCE", "Piece"),
    ("GR", "Gram"),
    ("HGR", "Hectogram"),
    ("KGR", "Kilogram"),
    ("L", "Litre"),
    ("ML", "Mililitre"),
    ("PCK", "Pack"),
    ("CAN", "Can"),
    ("BOT", "Bottle"),
)

class ProductCategory(models.Model):
    """A category of products, with a name"""
    category_name = models.CharField(_("Category name"), max_length=100)

    def __str__(self):
        return self.category_name

class Product(models.Model):
    """
    A product with a name, price, category and unit in which it should be counted.
    Is part of a House.
    """
    name = models.CharField(_("Product name"), max_length=100)
    price = models.DecimalField(_("Product price"), max_digits=6, decimal_places=2, default=0, help_text=_("Product price (CHF). Max 9999.99"))
    unit = models.CharField(_("Product unit"), max_length=3, choices=UNIT_CHOICES, default="PCE")

    house = models.ForeignKey(House, on_delete=models.CASCADE, null=True)
    category = models.ForeignKey(ProductCategory, null=True, on_delete=models.SET_NULL)

    created_at = models.DateTimeField(_("Product created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("Product updated at"), auto_now=True)
    is_active = models.BooleanField(_("Product deleted"), default=True)
