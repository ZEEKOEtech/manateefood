# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-08-28 17:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='deleted_at',
            field=models.DateTimeField(default=None, null=True, verbose_name='datetime deleted'),
        ),
        migrations.AlterField(
            model_name='productcategory',
            name='category_name',
            field=models.CharField(max_length=100, verbose_name='category name'),
        ),
    ]
