# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-08-29 13:37
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0005_auto_20170829_1410'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='unit',
            field=models.CharField(choices=[('PCE', 'Piece'), ('GR', 'Gram'), ('HGR', 'Hectogram'), ('KGR', 'Kilogram'), ('L', 'Liter'), ('ML', 'Mililiter'), ('PCK', 'Pack'), ('BOX', 'Box'), ('BOT', 'Bottle')], default='PCE', max_length=3, verbose_name='Product unit'),
        ),
        migrations.AlterField(
            model_name='product',
            name='price',
            field=models.DecimalField(decimal_places=2, default=0, help_text='Product price (CHF)', max_digits=6, verbose_name='Product price'),
        ),
    ]
